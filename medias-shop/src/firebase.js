import { initializeApp } from 'firebase/app';



const firebaseConfig = {
    apiKey: "AIzaSyAyYl1Ur5wZslwRacu4NLBxrR5LgceK7JE",
    authDomain: "cs476-4c4bb.firebaseapp.com",
    projectId: "cs476-4c4bb",
    storageBucket: "cs476-4c4bb.appspot.com",
    messagingSenderId: "1060608246624",
    appId: "1:1060608246624:web:3f88be99c4ca909aa3d405"
};

const firebaseApp = initializeApp(firebaseConfig);

const db = firebaseApp.firestore();
const auth = firebaseApp.auth();


export { db, auth };