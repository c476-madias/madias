import React, { useState } from 'react'
import { Link, useHistory } from 'react-router-dom'
import { auth } from './firebase'
import './Login.css'

function Login() {

    const history = useHistory();
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');

    const signIn = e => {

        e.preventDefault();

        auth
            .signInWithEmailAndPassword(email, password)
            .then(auth => {
                history.push('/')
            })
            .catch(erro => alert(erro.message));

    }

    return (
        <div className='login'>
            <Link to='/'>
                <img
                    className="login__logo"
                    src="https://i.imgur.com/E0HOpcY.png"
                    alt="madias Logo"
                />
            </Link>
            <div className="login__sigIn">
                <div className='login__container'>
                    <h1>Sign In</h1>
                    <form>
                        <h5>Email or mobile phone number</h5>
                        <input type="email" name="email" placeholder="Email" value={email}
                            onChange={e => setEmail(e.target.value)} />
                        <h5>Password</h5>
                        <input type="password" placeholder='Password' value={password} onChange={e => setPassword(e.target.value)} />
                        <button className="login__signInButton"
                            type="submit"
                            onClick={signIn}
                        >
                            Sign-In
                        </button>
                        <p>
                            By continuing, you agree to Madias Conditions of Use and Privacy Notice.
                        </p>
                    </form>
                </div>
                <div className='login__signUpPrompt'>
                    <p> New to Madias? </p>
                    <Link to='./SignUp'>
                        <button className='login__registerButton'
                        >
                            Create Your Madias Account
                        </button>
                    </Link>
                </div>
            </div>
        </div >
    )
}

export default Login
