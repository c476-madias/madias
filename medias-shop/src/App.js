import React, { useEffect } from 'react';
import './App.css';
import Home from './Home'
import Login from './Login'
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import { auth } from "./firebase";
import { useStateValue } from './StateProvider';
import SignUp from './SignUp';

function App() {
  const [{ }, dispatch] = useStateValue();

  useEffect(() => {
    auth.onIdTokenChanged(authUser => {
      console.log('THE_USE_IS >>>', authUser);
      if (authUser) {
        dispatch({
          type: 'SET_USER',
          user: authUser,
        })
      }
      else {
        dispatch({
          type: 'SET_USER',
          user: null,
        })
      }
    });
  }, [])
  return (
    <Router>
      <div className="app">

        <Switch>
          <Route path='/signup'>
            <SignUp />
          </Route>
          <Route path="/login">
            <Login />
          </Route>
          <Route path="/">
            <Home />
          </Route>
        </Switch>
      </div>
    </Router>
  );
}

export default App;
