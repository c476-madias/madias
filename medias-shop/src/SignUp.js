import React, { useState } from 'react'
import { Link, useHistory } from 'react-router-dom'
import { auth } from './firebase'
import './SignUp.css'
// import * as firebase from "firebase/app";
require('firebase/auth')
// import { getAuth, updateProfile } from "firebase/auth";

function SignUp() {

    const history = useHistory();
    const [name, setName] = useState('');
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [confirmPassword, setConfirmPassword] = useState('');

    const SignUp = e => {

        e.preventDefault();
        if (password !== confirmPassword) {
            alert("Passwords don't match");
        } else {
            auth
                .createUserWithEmailAndPassword(email, password)
                .then((auth) => {
                    auth().updateCurrentUser((user) => {
                        if (user) {
                            user.updateProfile({
                                displayName: name
                            })
                        }
                    })
                    // firebase.updateProfile(auth.currentUser, {
                    //     displayName: name
                    // })
                    if (auth) {
                        history.push('/')
                    }
                })
                .catch(error => alert(error.message));
        }
    }

    return (
        <div className='signup'>
            <Link to='/'>
                <img
                    className="signup__logo"
                    src="https://i.imgur.com/E0HOpcY.png"
                    alt="Madias Logo"
                />
            </Link>
            <div className="signup__sigIn">
                <div className='signup__container'>
                    <h1>Create Your Account</h1>
                    <form>
                        <h5>Your name</h5>
                        <input type='text' name="name" placeholder="Name" value={name}
                            onChange={e => setName(e.target.value)} />
                        <h5>Email or mobile phone number</h5>
                        <input type="email" name="email" placeholder="Email" value={email}
                            onChange={e => setEmail(e.target.value)} />
                        <h5>Password</h5>
                        <input type="password" placeholder='At least 6 characters' value={password} onChange={e => setPassword(e.target.value)} />
                        <h5>Re-enter password</h5>
                        <input type="password" placeholder='confirm password' value={confirmPassword} onChange={
                            e => setConfirmPassword(e.target.value)
                        } />
                        <button className="signup__signInButton"
                            type="submit"
                            onClick={SignUp}
                        >
                            Sign-Up
                        </button>
                        <p>
                            By continuing, you agree to Madias Conditions of Use and Privacy Notice.
                        </p>
                        <hr />
                        <p> Already have an account?
                            <a href="/login">Sign In</a>
                        </p>
                    </form>
                </div>
            </div>
        </div >
    )
}

export default SignUp
